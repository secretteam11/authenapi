# Authentication API

This API supports user management and also session management. 


## User management 

* GET /api/user/        get all users
* GET /api/user/{id}      get user with id
* POST /api/user/       create new user

payload: 
```Javascript
    {
        "Name": "{username}",
        "Password": "{password}"
    }
```

*  PUT /api/user/{id}    update user with id

    payload: 
```Javascript
    {
        "Name": "{username}",
        "Password": "{password}"
    }
```

* DELETE /api/user/{id}     delete user with id

## Session management

* GET /api/session/         get all active sessions
* GET /api/session/{id}     get session with id
* POST /api/session/        create new session with username/password

    payload: 
```Javascript
    {
        "Name": "{username}",
        "Password": "{password}"
    }
```
* DELETE /api/session/{id}  remove session